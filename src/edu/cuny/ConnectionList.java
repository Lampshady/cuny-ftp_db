package edu.cuny;

import java.util.ArrayList;
import java.util.Collections;

import edu.cuny.ftp.FTPSession;
import edu.cuny.parser.FileHandler;
import edu.cuny.parser.FileHandlerHelper;
import edu.cuny.parser.HTMLizer;

public class ConnectionList {

	private ArrayList<FTPSession> connectionList;
	private ArrayList<String> messageList, errorList;
	private String destination;
	private boolean checkCurrent;
	
	public ConnectionList(ArrayList<FTPSession> connectionList){
		this.connectionList = connectionList;
		messageList = new ArrayList<String>();
		errorList = new ArrayList<String>();
	}
	
	public String run(){
		for (FTPSession connection : connectionList){
			String remoteFile;
			checkCurrent= true;
			FileHandler downloadedFiles = new FileHandler(checkCurrent);
			
			
			//Downloads file from FTP servers. If error occurs, connectionLog is sent.
			if (connection.getData()){
					remoteFile = connection.getMyFTPFile().getFilename();
					if(downloadedFiles.loadFiles(remoteFile) && downloadedFiles.netMoveFiles(destination)){
						unionMessages(HTMLizer.parseData(downloadedFiles));
					}	
					else
						errorList.add("<p>"+downloadedFiles.getFileLog()+"</p>");
						
					FileHandlerHelper.clearAllFiles(remoteFile);
			}
			else
				errorList.add("<p>"+connection.getConnectionLog()+"</p>");
		}
		
		if (messageList.isEmpty())
			return arrayToString(errorList);
		
		Collections.sort(messageList);
		messageList.add(0,HTMLizer.TABLE);
		messageList.add(HTMLizer.CLOSETABLE);
		messageList.addAll(errorList);
		return arrayToString(messageList);
	}

	private void unionMessages(String[] messages){
		for (String message : messages)
			messageList.add(message);
	}
	

	public String getDestination() {
		return destination;
	}
	public void setDestination(String destination) {
		this.destination = destination;
	}
	public boolean isCheckCurrent() {
		return checkCurrent;
	}
	public void setCheckCurrent(boolean checkCurrent) {
		this.checkCurrent = checkCurrent;
	}

	private String arrayToString(ArrayList<String> arrayList){
		String listString = "";
		for (String s : arrayList)
		    listString += s;
		
		return listString;
		
	}
	
	
}
