package edu.cuny.ftp;

public class FTPSite {

	private String url;
	private String security;
	private Integer port;
	private String user;
	private String pass;
	
	
	@Override
	public String toString() {
		return "FTPSite [url=" + url + ", port=" + port + ", user=" + user
				+ ", pass=" + pass + "]";
	}
	
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public Integer getPort() {
		return port;
	}
	public void setPort(Integer port) {
		this.port = port;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	public String getSecurity() {
		return security;
	}
	public void setSecurity(String security) {
		this.security = security;
	}
	
	
}
