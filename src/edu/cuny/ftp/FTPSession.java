package edu.cuny.ftp;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

import it.sauronsoftware.ftp4j.FTPAbortedException;
import it.sauronsoftware.ftp4j.FTPClient;
import it.sauronsoftware.ftp4j.FTPDataTransferException;
import it.sauronsoftware.ftp4j.FTPException;
import it.sauronsoftware.ftp4j.FTPIllegalReplyException;

public class FTPSession {
	private String connectionLog;
	private FTPClient client;
	private FTPSite myFTPSite;
	private FTPFile myFTPFile;
	private FTPTransferListener transferListener = new FTPTransferListener();
	
	private boolean complete = false;

	FTPSession(){
		connectionLog = "";
		client = new FTPClient();
		transferListener = new FTPTransferListener();
	}

	public boolean getData() {
		
		if(connect())
			if(checkFile())
				if(downloadFile())
					complete = true;

		closeConnection();
		return complete;
	}
	
	public boolean connect(){
		
		try {
			//Configures FTP/FTPS/FTPES and sets up certificate
			FTPSessionHelper.applySecurity(client, myFTPSite.getSecurity());
			
			if (myFTPSite.getPort() == null)
				client.connect(myFTPSite.getUrl());
			else
				client.connect(myFTPSite.getUrl(), myFTPSite.getPort());
			
			client.login(myFTPSite.getUser(), myFTPSite.getPass());
		} catch (IllegalStateException e) {
			/* 	E1
			 * 	Description- FTP4J Does not have a connection configured
			 * 	Issues: Any connection issues
			 */
			connectionLog += "Connection Error: Settings are not configured properly.\n";
			connectionLog += "\tPlease contact your friendly neigborhood developer.\n \n";
			return false;
		} catch (IOException e) {
			/*	E2
			 * 	Description- Any connection issue with the server once it is found
			 *	Issues: 
			 *	Wrong URL:  detailedMessage=� connect timed out�
			 *	Wrong Port: detailedMessage=� Connection refused: connect�
			 */
			connectionLog += "Connection Error: "+e.getMessage()+"\n";
			connectionLog += "\tPlease verify url and port.\n  \n";
			return false;
		} catch (FTPIllegalReplyException e) {
			/* 	E3
			 * 	Description: This one means that the remote server has replied in an illegal way, that is not FTP compliant. That should be very rare.
			 * 	Issues:  FTP Server is not what it appears to be
			 */
			connectionLog += "Connection Error:  FTP Server has responeded in an illegal way.\n";
			connectionLog += "\tPlease contact FTP admin.\n \n";
			return false;
		} catch (FTPException e) {
			/* 	E4
			 * 	Description: Credential failure.
			 *	Issue: code=530
			 *	Using SSL account without using setting SSL: message=� SSL required�
			 *	Wrong username: message=�  Login or password incorrect!�
			 */
			connectionLog += "Connection Error:  Connection configured incorrectly.\n";
			if(e.getMessage().contains("SSL required") )connectionLog += "\tSSL is not configured properly.\n \n";
			else connectionLog += "\tInvalid username or password.\n \n";
			return false;			
		}
		
		return true;
	}
		
	
	public boolean checkFile(){
		boolean isUpdated = false;
		
		try {
			Date remoteDate = client.modifiedDate(myFTPFile.getFilename());
			isUpdated = myFTPFile.isFileUpdated(remoteDate);
		} catch (IllegalStateException e) {
			/*	E5
			 * 	Description- FTP4J Does not have a connection configured
			 *	Issues: Usually follows E2 errors
			 */	
			connectionLog += "Connection Error: Settings are not configured properly.\n";
			connectionLog += "\tPlease contact your friendly neigborhood developer.\n \n";
			return false;
		} catch (IOException e) {
			System.out.println("Error 6");
			/*	E6
			 */
			connectionLog += "Unknown Error: E6";
			return false;
		} catch (FTPIllegalReplyException e) {
			/* 	E7
			 * 	Description: This one means that the remote server has replied in an illegal way, that is not FTP compliant. That should be very rare.
			 * 	Issues:  FTP Server is not what it appears to be
			 */
			connectionLog += "Session Error:  FTP Server has responeded in an illegal way.\n";
			connectionLog += "\tPlease contact FTP admin.\n \n";
			return false;
		} catch (FTPException e) {
			/*	E8
			 * 	Description: Error during session
				File not found
					message=� File not found�
					code=550
				Kicked off ftp
					message=� Kicked by Administrator�
					code=421
				Server shut down
					message=� Server is going offline�
					code=421
			 */
			if (e.getMessage().contains("Kicked by")){
				connectionLog += "Server Error: Session has been termiated by administrator.\n";
				connectionLog += "\tPlease contact FTP admin.\n \n";
			}
			else if (e.getMessage().contains("Server is going offline")){
				connectionLog += "Server Error: Server has been shutdown by administrator.\n";
				connectionLog += "\tPlease contact FTP admin.\n \n";
			}
			else if (e.getCode() == 550){
				connectionLog += "File Error: File not found on remote machine.\n";
				connectionLog += "\tPlease verify filename.\n \n";
			}
			else {
				connectionLog += "Unknown Error: E8 \n \n";
			}
			return false;
		}
		
		return isUpdated;
	}
	
	public boolean downloadFile(){		
		
		try {
			File localFile = new File(myFTPFile.getFilename());
			client.download(myFTPFile.getFilename(), localFile, transferListener);
		} catch (IllegalStateException e) {
			/*	E9
			 * 	Description- FTP4J Does not have a connection configured
			 *	Issues: Usually follows E2 errors
			 */	
			connectionLog += "Connection Error: Settings are not configured properly.\n";
			connectionLog += "\tPlease contact your friendly neigborhood developer.\n \n";
			return false;
		} catch (FileNotFoundException e) {
			connectionLog += "File Error E10: Please contact your friendly neigborhood developer.\n \n";
			return false;
		} catch (IOException e) {
			//	E11
			connectionLog += "Unknown Error: E11.\n \n";;
			return false;
		} catch (FTPIllegalReplyException e) {
			/* 	E12
			 * 	Description: This one means that the remote server has replied in an illegal way, that is not FTP compliant. That should be very rare.
			 * 	Issues:  FTP Server is not what it appears to be
			 */
			connectionLog += "Session Error:  FTP Server has responeded in an illegal way.\n";
			connectionLog += "\tPlease contact FTP admin.\n \n";
			return false;
		} catch (FTPException e) {
			//E13
			if (e.getMessage().contains("Kicked by")){
				connectionLog += "Server Error: Session has been termiated by administrator.\n";
				connectionLog += "\tPlease contact FTP admin.\n \n";
			}
			else if (e.getMessage().contains("Server is going offline")){
				connectionLog += "Server Error: Server has been shutdown by administrator.\n";
				connectionLog += "\tPlease contact FTP admin.\n \n";
			}
			else {
				connectionLog += "Unknown Error: E13.\n \n";
			}
			return false;
		} catch (FTPDataTransferException e) {
			//	E14
			connectionLog += "Unknown Error: E14.\n \n";
			return false;
		} catch (FTPAbortedException e) {
			//	E15
			connectionLog += "Unknown Error: E15.\n \n";
			return false;
		}

		return true;
	}
	
	public void closeConnection(){
			
		try {
			if (client.isConnected())
				client.disconnect(false);
		} catch (IllegalStateException | IOException | FTPIllegalReplyException
				| FTPException e) {
			System.out.println("Connection Close Error: "+e.getMessage());
		}
	
		
	}
	
	public String toString() {
		String display = "The Site:\n "+ myFTPSite + "\n";
		display += "The File:\n "+ myFTPFile + "\n";
		return display;
	}

	
	public FTPTransferListener getTransferListener() {
		return transferListener;
	}
	public void setTransferListener(FTPTransferListener transferListener) {
		this.transferListener = transferListener;
	}
	public FTPClient getClient() {
		return client;
	}
	public void setClient(FTPClient client) {
		this.client = client;
	}	
	public FTPSite getMyFTPSite() {
		return myFTPSite;
	}
	public void setMyFTPSite(FTPSite myFTPSite) {
		this.myFTPSite = myFTPSite;
	}
	public FTPFile getMyFTPFile() {
		return myFTPFile;
	}
	public void setMyFTPFile(FTPFile myFTPFile) {
		this.myFTPFile = myFTPFile;
	}
	public String getConnectionLog() {
		return connectionLog;
	}
	public void setConnectionLog(String connectionLog) {
		this.connectionLog = connectionLog;
	}

	public boolean isComplete() {
		return complete;
	}
	
	
	
}
