package edu.cuny.ftp;
//import java.util.Date;

import it.sauronsoftware.ftp4j.FTPDataTransferListener;

public class FTPTransferListener implements FTPDataTransferListener {
	
	
	@Override
	public void started() {
		
	}

	@Override
	public void transferred(int length) {
		
	}

	@Override
	public void completed() {
		
	}
	
	@Override
	public void aborted() {
		
	}
	
	@Override
	public void failed() {
		
	}

}