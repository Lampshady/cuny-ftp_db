package edu.cuny.ftp;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import it.sauronsoftware.ftp4j.FTPClient;

public class FTPSessionHelper {

	public static void applySecurity(FTPClient client, String security){
		SSLSocketFactory sslSocketFactory = certHandler();
		
		//Applies secure connection if specified
		if (security.equals("FTPS"))
			client.setSecurity(FTPClient.SECURITY_FTPS);
		else if (security.equals("FTPES"))
			client.setSecurity(FTPClient.SECURITY_FTPES);
		else{}
		//TODO verify FTP, report wrong option
		
		client.setSSLSocketFactory(sslSocketFactory);
	}
	
	//Certificate Handler - Learn how to use
	public static SSLSocketFactory certHandler(){
		TrustManager[] trustManager = new TrustManager[] { new X509TrustManager() {
			public X509Certificate[] getAcceptedIssuers() {
				return null;
			}
			public void checkClientTrusted(X509Certificate[] certs, String authType) {}
			public void checkServerTrusted(X509Certificate[] certs, String authType) {}
		} };
		SSLContext sslContext = null;
		try {
			sslContext = SSLContext.getInstance("SSL");
			sslContext.init(null, trustManager, new SecureRandom());
		} catch (NoSuchAlgorithmException e) {
			//TODO SSL Catch
			e.printStackTrace();
		} catch (KeyManagementException e) {
			//TODO SSL Catch
			e.printStackTrace();
		}
		return sslContext.getSocketFactory();
	}
	
}
