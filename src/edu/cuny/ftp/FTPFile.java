package edu.cuny.ftp;


import java.util.Date;
import java.sql.Timestamp;

public class FTPFile {
	private String dir;
	private String filename;
	private Date lastModifiedDate;
	private Date remoteDate;

	//FTPFile(String dir, String remoteFilename, String localFilename){
	 FTPFile(){
		//TODO - Recall modified date from last transfer
	 	this.lastModifiedDate = new Date();
	 	this.remoteDate = new Date();
	}
	
	public String getDir() {
		return dir;
	}
	public void setDir(String dir) {
		this.dir = dir;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public Date getLastModifiedDate() {
		return lastModifiedDate;
	}
	public void setLastModifiedDate(Date lastModifiedDate) {
		this.lastModifiedDate = lastModifiedDate;
	}
	public Date getRemoteDate() {
		return remoteDate;
	}
	public void setRemoteDate(Date remoteDate) {
		this.remoteDate = remoteDate;
	}

	
	public boolean isFileUpdated(Date remoteDate){
		this.remoteDate = remoteDate;
		//return lastModifiedDate.before(remoteDate);
		return true;
	}
	
	
	
	public String toString() {
		String display = "Directory: "+ dir + "\n";
		display += "Remote File Name: "+ filename + "\n";
		display += "Last Modified Date: "+ new Timestamp(lastModifiedDate.getTime()) + "\n";
		display += "Remote Modified Date: "+ new Timestamp(remoteDate.getTime()) + "\n";
		
		return display;
	}
}