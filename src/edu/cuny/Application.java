package edu.cuny;


import org.springframework.context.support.AbstractApplicationContext;
//import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import edu.cuny.email.EmailSender;

public class Application {

	public static void main(String[] args) {
		String message = "";
		AbstractApplicationContext context;
		
		context = new FileSystemXmlApplicationContext("ftp.spring.xml");
		context.registerShutdownHook();
		ConnectionList connectionList = (ConnectionList)context.getBean("connectionList");

		message = connectionList.run();
		
		context.close();
		
		
		context = new FileSystemXmlApplicationContext("email.spring.xml");
		context.registerShutdownHook();
		EmailSender emailConnection = (EmailSender)context.getBean("emailConnection");
		
		emailConnection.sendMessage(message);
		context.close();
	}

}


