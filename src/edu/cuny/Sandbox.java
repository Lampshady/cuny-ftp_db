package edu.cuny;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import edu.cuny.parser.FileHandler;
import edu.cuny.parser.FileHandlerHelper;
import edu.cuny.parser.HTMLizer;

@SuppressWarnings("unused")
public class Sandbox {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String zip = "VWB_360MARC_Update_2013-05-10.zip";
		ArrayList<String> messageList = new ArrayList<String>();
		
		FileHandler allFiles = new FileHandler(true);
		allFiles.loadFiles(zip);

		String[] htmlizedData = HTMLizer.parseData(allFiles);
		
		//for (String line : htmlizedData)
			//messageList.add(line);
		
		for (String line : htmlizedData)
			System.out.println(line);
		
		return;
	}

}
