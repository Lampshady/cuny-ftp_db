package edu.cuny.email;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import javax.mail.*;
import javax.mail.internet.*;

public class EmailSender {
	public static final DateFormat outputFormat = new SimpleDateFormat("MM/yy");
	public static final DateFormat fullFormat = new SimpleDateFormat("dd/MM/yyyy");
      
	private String[] toAddress;
	private String fromAddress;
	private String smtpHost;
	private String smtpPort;

	private Properties properties ;
	private Session session;
	
	private String subject;
	private String format;
	private String customMessage;

	
	public EmailSender(String smtpHost, String smtpPort) {
		format = "text/html";
		this.smtpHost = smtpHost;
		this.smtpPort = smtpPort;
			
		properties = System.getProperties();
		properties.setProperty("mail.smtp.host", this.smtpHost);
		properties.setProperty("mail.smtp.port", this.smtpPort);
	}
 
	
	public boolean sendMessage(String messageList){
		session = Session.getDefaultInstance(properties);
		String messageContent = "<p>"+customMessage+"</p>"+messageList;
		
		try {
			// Create a default MimeMessage object.
			MimeMessage message = new MimeMessage(session);
			
			//Builds email message
			message.setFrom(new InternetAddress(fromAddress));
			message.addRecipients(Message.RecipientType.TO, convertTo());
			message.setSubject(subject+" - "+outputFormat.format(new java.util.Date()));
						

			message.setContent(messageContent, format);

			// Send message
			Transport.send(message);
		} catch (MessagingException mex) {
			System.out.println("Message Failed!  "+mex.getLocalizedMessage());
			return false;
		}
		
		
		System.out.println("Sent message successfully....");
		return true;
	}

	private InternetAddress[] convertTo(){
		InternetAddress[] addressTo = new InternetAddress[toAddress.length];
        for (int i = 0; i < toAddress.length; i++)
			try {
				addressTo[i] = new InternetAddress(toAddress[i]);
			} catch (AddressException e) {
				return null;
			}
        
        return addressTo;
	}


	public String[] getToAddress() {
		return toAddress;
	}
	public void setToAddress(String[] toAddress) {
		this.toAddress = toAddress;
	}
	public String getFromAddress() {
		return fromAddress;
	}
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getCustomMessage() {
		return customMessage;
	}
	public void setCustomMessage(String customMessage) {
		this.customMessage = customMessage;
	}
	
	

}
