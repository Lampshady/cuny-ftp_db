package edu.cuny.parser;

public class HTMLizer {
	public static final String TABLE = "<table style='border: 1px solid #000; border-collapse: collapse'>";
	public static final String CLOSETABLE = "</table>";
	public static final String TD = "<td style='border: 1px solid #000;'>";
	private static final String[] TYPES = {"chg", "new", "del"};
	
	
	public static String[] parseData(FileHandler fileHandler){
		String htmlMessage = "";
		SummaryFile summaryFile = fileHandler.getSummaryFile();
		int[][] data = {summaryFile.chgRecords,summaryFile.newRecords,summaryFile.delRecords};
		int t = 0;
		
		for (String type : TYPES){
			for (int x = 0;x<summaryFile.chgRecords.length;x++){
				htmlMessage += "<tr>"+TD+type+summaryFile.cunyCode+summaryFile.getDate()+"_"+(x+1)+".mrc"+"</td>"+TD+data[t][x]+"</td></tr>|";
			}
			t++;
		}
		
		return htmlMessage.split("\\|");
		
	}
	

}
