package edu.cuny.parser;

import java.io.File;
import java.io.IOException;
import net.lingala.zip4j.core.ZipFile;
import net.lingala.zip4j.exception.ZipException;

public class FileHandlerHelper {
	private static final String[] vendorCodes	= {	"ZCY","VVB","VDB","XMC","VWB","ZXC","XQL",
													"ZGM","JOU","HC5","ZHM","VVJ","NYK","VYL",
													"CTX","XME","ZNC","XQM","VSI","VMY"};
	private static final String[] cunyCodes 	= {	"al", "bb", "bc", "bm", "bx", "cc", "cl",
													"gc", "gj", "ho", "hc", "jj", "kb", "le",
													"lg", "me", "ny", "qc", "si", "yc" };
	
	public static boolean unzipFiles(String filename){
			//String password = "";
			String zipDirectory = filename.substring(0, filename.lastIndexOf('.'));
			ZipFile zipFile;
			
			//Extracts contents of zip to folder
			try {
				zipFile = new ZipFile(filename);
				//if (zipFile.isEncrypted()) zipFile.setPassword(password);

				zipFile.extractAll(zipDirectory);
			} catch (ZipException e) {
				return false;
			}
					
			return true;
		}
	
	public static String renameFile(String filename, String dateStamp){
		String type;
		String prefix;
		String action;
		String oldCode;
		int index;
		
		if((index = filename.lastIndexOf("_")) < 0) return null;
		type = filename.substring(index);
		prefix = filename.substring(0,index);
		
		if((index = prefix.lastIndexOf("_")) < 0) return null;
		action = prefix.substring(index+1);
		if(action.compareToIgnoreCase("changed")==0)action = "chng";
		else if(action.compareToIgnoreCase("deleted")==0)action = "del";
		
		index = filename.indexOf("_");
		oldCode = filename.substring(0,index);
		
		return action+getCode(oldCode)+dateStamp+type;
		
	}
	
	public static String getCode(String oldCode){
		for(int x=0; x < vendorCodes.length; x++) 
			if (vendorCodes[x].compareToIgnoreCase(oldCode) == 0) return cunyCodes[x];
		return null;
	}

	//Checks the file type
	public static boolean isFileType(String filename, String type){
		String fileExt, f;
		int index;
		
		if((index = filename.lastIndexOf('\\')) >= 0)
			f = filename.substring(index+1);
		else 
			f = filename;
		
		//Folder
		if((index = f.lastIndexOf(".")) < 0) return false;
		//File Type
		fileExt =  f.substring(index);
		if(fileExt.compareToIgnoreCase(type) != 0 )return false;
		return true;	
	}
	
	public static int fileCount(String filename){
		File dir = new File(filename);
		return dir.list().length;
	}
	
	//---------------------------- Deletion Section -----------------
	
	//Deletes zip file and the extracted files
	public static boolean clearAllFiles(String filename){
		File file = new File(filename);
		file.delete();
		
		clearFiles(filename);
		
		return true;
	}
	
	//Deletes the extracted files from the zip
	public static boolean clearFiles(String filename){
		File directory = new File(filename.substring(0, filename.lastIndexOf('.')));
		 
    	//make sure directory exists
    	if(!directory.exists())
    		return true;
    	
		try {
			deleteFile(directory);
		} catch (IOException e){
			return false;
		}
		
		return true;
		
	}
	
	//Recurses through a directory to delete files
	private static void deleteFile(File file) throws IOException {
		String filenames[];
				
		if (file.isDirectory()) {
			
			if (file.list().length == 0) {
				file.delete();
			} else {
				filenames = file.list();
				for (String fname : filenames)
					deleteFile(new File(file, fname));

				if (file.list().length == 0) 
					file.delete();
			}

		} else  //(file.isDirectory())
			file.delete();
	}
}
