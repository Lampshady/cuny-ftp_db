package edu.cuny.parser;

import java.io.File;
import java.nio.file.Files;
import java.util.ArrayList;

import org.apache.commons.io.FileUtils;

import static java.nio.file.StandardCopyOption.*;

public class FileHandler {
	private String summaryFilename = "runsummary";
	private String mrcZipFilename = "update_serials.zip";
	//private static final String remoteDirectory = "CUNYfiles";
	

	private ArrayList<File> mrcList;
	private SummaryFile summaryFile;
	private String fileLog;
	private boolean checkCurrent;

	public FileHandler(boolean checkCurrent){
		mrcList = new ArrayList<File>();
		summaryFile = null;
		fileLog = "";
		this.checkCurrent = checkCurrent;
	}
	
	//UNloads all zip files into list
	public boolean loadFiles(String filename){
		//Verifies there are no existing directories with the zip file name
		if (!FileHandlerHelper.clearFiles(filename)){
			fileLog += "File Error: Could not clear existing files!";
			return false;
		}
		
		//Adds the the file and unpacks zips
		if(!unpackFiles(filename))return false;
		
		return true;
	}

	public boolean netMoveFiles(String destination){
		destination += File.separator;
		for (File file : mrcList){
			String filename = destination+file.getName();
			try{
				FileUtils.moveFile(file,new File(filename));
				//Files.copy(file.toPath(), new File(filename).toPath(), REPLACE_EXISTING);	
			}
			catch(Exception e){
				fileLog += "File Move Error: Cannot write to '" + destination + "'.\n";
				return false;
			}
		}
		return true;
	}
	
	private boolean unpackFiles(String filename){
		//Unzips or adds Files
		String mrcZip =null;
		String rootDir, mrcDir;
		
		//Checks if root file is a zip
		if(!FileHandlerHelper.isFileType(filename, ".zip")){
			fileLog += "File Error: '"+ filename +"' must be a zip file.\n";
			return false;
		}
		
		
		//Unpacks the root zip file and loads the summary and mrc zipfile
		
		if(!FileHandlerHelper.unzipFiles(filename))
			fileLog += "Zip Error: '" + filename + "' cannot be accessed.\n";
		rootDir = filename.substring(0, filename.lastIndexOf('.'));
		if (FileHandlerHelper.fileCount(rootDir) < 1)
			fileLog += "Zip Error: '" + filename + "' has non contents.\n";
		if(!fileLog.equals(""))return false;
		
		//Goes through each file in the zip directory
		for (String fname : new File(rootDir).list()){
			fname = rootDir+"\\"+fname;
			if (fname.toLowerCase().contains(summaryFilename))
				summaryFile = new SummaryFile(new File(fname));
				
			else if (fname.toLowerCase().contains(mrcZipFilename))
				mrcZip = new String(fname);
		}
		
		//Checks if summary and mrc exist and handles error
		if(summaryFile == null){
			fileLog += "Zip Error: '" + filename + "' doesn not contain a summary file.\n";
			return false;
		}
		if(mrcZip == null){
			fileLog += "Zip Error: '" + filename + "' doesn not contain '.mrc' files.\n";
			return false;
		}
		
		
		//Builds summary info in summary object
		if(!summaryFile.buildSummary()){
			fileLog += "Content Error: Summary file is not configured properly.\n";
			return false;
		}
		
		if(checkCurrent && !summaryFile.isCurrent()){
			fileLog += "Content Error: Summary file for '"+summaryFile.cunyCode+"' is not current.\n";
			return false;
		}
		
		summaryFile.cunyCode = FileHandlerHelper.getCode(rootDir.substring(0, 3));
		
		//UNpacks the mrcZip file and performs etl on files
		if(!FileHandlerHelper.unzipFiles(mrcZip))
			fileLog += "Zip Error: '" + filename + "' cannot be accessed.\n";
		mrcDir = mrcZip.substring(0, mrcZip.lastIndexOf('.'));
		if (FileHandlerHelper.fileCount(mrcDir) < 1)
			fileLog += "Zip Error: '" + filename + "' has non contents.\n";
		if(!fileLog.equals(""))return false;
		
		//formats and loads mrc files
		File newFile, file;
		for (String fname : new File(mrcDir).list())
			if (FileHandlerHelper.isFileType(fname, ".mrc")){
				file = new File(mrcDir+"//"+fname);
				newFile = new File(mrcDir+"//"+FileHandlerHelper.renameFile(fname, summaryFile.getDate()));
				try{
					Files.copy(file.toPath(), newFile.toPath(), REPLACE_EXISTING);
					mrcList.add(newFile);
				}
				catch(Exception e){
					return false;
				}
				
			}
		

		return true;
	} 
	
	
	public ArrayList<File> getFileList() {
		return mrcList;
	}

	public SummaryFile getSummaryFile() {
		return summaryFile;
	}

	public String getFileLog() {
		return fileLog;
	}
	
	
	
	
	
}

