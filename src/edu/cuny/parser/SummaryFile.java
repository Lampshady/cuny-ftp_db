package edu.cuny.parser;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;

public class SummaryFile {
	public static final DateFormat inputFormat = new SimpleDateFormat("MM/dd/yyyy hh:mm a");
	public static final DateFormat outputFormat = new SimpleDateFormat("MMyy");
	public static final int MAXRECORDS = 5000;
	
	private File summaryFile;
	
	private Date summaryDate;
	
	public String cunyCode;
	public int[] newRecords;
	public int[] chgRecords;
	public int[] delRecords;
	
	public SummaryFile(File file){
		summaryFile = file;
		summaryDate = null;
		cunyCode = null;
		newRecords = null;
		chgRecords = null;
		delRecords = null;
	}
	
	public String getDate(){
		return outputFormat.format(summaryDate);
	}
	
	public boolean buildSummary() {
		BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(summaryFile));
			String line;
			while ((line = br.readLine()) != null){
				
				//Handles Summary Date
				if (line.contains("Report Ran"))
					summaryDate = populateDate(line);

				//Handles Record numbers
				else if (line.contains("All New Records"))
					newRecords = populateArray(line);
				
				else if (line.contains("All Changed Records"))
					chgRecords = populateArray(line);
				
				else if (line.contains("All Deleted Records"))
					delRecords = populateArray(line);
				

			}
			br.close();
			
		} catch (Exception e) {
			return false;
		}
		
		if( (summaryDate == null) || (newRecords == null) || (chgRecords == null) || (delRecords == null) )return false;
		
		return true;
	}
	
	public boolean isCurrent(){
		if(outputFormat.format(summaryDate).equalsIgnoreCase(outputFormat.format(new java.util.Date())) )
			return true;
		else return false;
	}

	private int[] populateArray(String line){
		String extract = line.substring(line.indexOf(":")+1).trim();
		int count = Integer.parseInt(extract);
		int[] array = new int[(count/MAXRECORDS)+1];
		Arrays.fill(array, MAXRECORDS);
		array[array.length-1] = count%MAXRECORDS;
		return array;
	}
	
	private Date populateDate(String line){
		String extract = line.substring(line.indexOf(":")+1).trim();
	    try {
	    	return inputFormat.parse(extract);
	    } catch (ParseException e) {
	        return null;
	    }
	}

}
